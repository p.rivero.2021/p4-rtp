## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: 1268
* ¿Cuánto tiempo dura la captura?: 12,81 segundos
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 99 paquetes
* ¿Qué protocolos de nivel de red aparecen en la captura?: IP
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 384
* Dirección IP de la máquina A: 216.234.64.16
* Dirección IP de la máquina B: 192.168.0.10
* Puerto UDP desde el que se envía el paquete: 54550
* Puerto UDP hacia el que se envía el paquete: 49154
* SSRC del paquete: 0x31be1e0e
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): ITU-T G.711 PCMU (0)
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?: 5
* ¿Cuántos paquetes hay en el flujo F?: 642
* ¿El origen del flujo F es la máquina A o B?: B
* ¿Cuál es el puerto UDP de destino del flujo F?: 54550
* ¿Cuántos segundos dura el flujo F?: 12,81 segundos
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 12,83 ms
* ¿Cuál es el jitter medio del flujo?: 12,23 ms
* ¿Cuántos paquetes del flujo se han perdido?: 0
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: 12,72 segundos
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 18627 
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: Tarde, porque está en positivo
* ¿Qué jitter se ha calculado para ese paquete?: 0,1646 ms
* ¿Qué timestamp tiene ese paquete?: 1769336203
* ¿Por qué número hexadecimal empieza sus datos de audio?: 3
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: mié 18 oct 2023 20:05:40
* Número total de paquetes en la captura: 2545 paquetes
* Duración total de la captura (en segundos): 6,54 segundos
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?: 450 paquetes
* ¿Cuál es el SSRC del flujo?: 0xb5bb9ae4
* ¿En qué momento (en ms) de la traza comienza el flujo?: 0,00138 segundos
* ¿Qué número de secuencia tiene el primer paquete del flujo?: 
* ¿Cuál es el jitter medio del flujo?: 0 segundos
* ¿Cuántos paquetes del flujo se han perdido?: 0 paquetes
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?:
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura):
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?:
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?:
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?:
